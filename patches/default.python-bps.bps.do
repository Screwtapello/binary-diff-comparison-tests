# Produce a blip patch with python-blip

redo-ifchange util.rc
. ./util.rc

# The official bps tool refuses to read from or write to a patch whose filename
# does not end in ".bps". GAR.

log "Creating patch..."
bps-diff "$SOURCE" "$TARGET" > "$3".bps

log "Validating patch..."
bps-validate "$3".bps

NEWTARGET=$(mktemp)

log "Applying patch with official tool..."
bps-reference apply "$3".bps "$SOURCE" "$NEWTARGET" 1>&2
compare_files "$TARGET" "$NEWTARGET"

log "Applying patch with python-bps..."
bps-apply "$SOURCE" "$NEWTARGET" "$3".bps
compare_files "$TARGET" "$NEWTARGET"

mv "$3".bps "$3"
