# Produce a VCDIFF format patch using Xdelta 3 with no internal compression.

redo-ifchange util.rc
. ./util.rc

log "Creating patch..."
xdelta3 -0 -f -e -s "$SOURCE" "$TARGET" "$3"

log "Applying patch..."
NEWTARGET=$(mktemp)
xdelta3 -f -d -s "$SOURCE" "$3" "$NEWTARGET"

# Make sure the target is reconstructed correctly.
compare_files "$TARGET" "$NEWTARGET"
