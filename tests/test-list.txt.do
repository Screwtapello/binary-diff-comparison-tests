# Since we can't otherwise detect tests added or removed, make sure this script
# is always run.
redo-always

# Print a list of subdirectories that contain source and target files.
for name in *; do
	# If it's not a directory, we're not interested.
	[ ! -d "$name" ] && continue

	# If there's not a "source" or "source.do" file, we're not interested.
	[ ! \( -e "$name/source" -o -e "$name/source.do" \) ] && continue

	# If there's not a "target" or "target.do" file, we're not interested.
	[ ! \( -e "$name/target" -o -e "$name/target.do" \) ] && continue

	# OK, this looks like a legit test.
	echo "$name"
done | sort > "$3"

# If the list hasn't changed since last time, don't rebuild any downstream
# products.
redo-stamp < "$3"
