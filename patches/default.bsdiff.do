# Produce a bsdiff patch.

redo-ifchange util.rc
. ./util.rc

log "Creating patch..."
bsdiff "$SOURCE" "$TARGET" "$3"

log "Applying patch..."
NEWTARGET=$(mktemp)
bspatch "$SOURCE" "$NEWTARGET" "$3"

# Make sure the target is reconstructed correctly.
compare_files "$TARGET" "$NEWTARGET"
