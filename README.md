Delta Encoding Test Framework
=============================

This is a framework designed to make it easy to test a number of delta encoding
tools against a number of test-cases, and produce a nicely-formatted report at
the end.

Running the test corpus
=======================

This framework is based on the [redo] [1] build system. If you have redo
installed, just run:

    $ redo

...and all tools will be run against all the tests, producing a report named
`results.tsv`. If you run it again, only files whose dependencies have changed
will be rebuilt.

If you don't have redo, the included `do` script will interpret the build
scripts, but it doesn't support dependency management - it rebuilds every
target every time.

Adding tests
============

To add a test, create a subdirectory in the `tests/` directory, containing
a file named `source` and a file named `target`. The subdirectory's name may
contain any printable characters besides "." (a full-stop). Any files not named
`source` or `target` in the subdirectory will be ignored, so you can copy in
files with sensible names and just symlink them to `source` and `target`.

Adding delta encoding tools
===========================

To include a new delta encoding tool, you must teach the framework how to run
the tool, and add the tool to the list of patch types. Assuming you're adding
a tool called FooDiff that produces `.foo` files:

1. Create a shell script named `patches/default.foo.do` that invokes FooDiff on
   source and target files. You might want to look at the other `default.*.do`
   files for examples.
   * Make sure you `redo-ifchange` the `util.rc` file and source it.
2. Add the line `foo` to `config/patch-types.txt`.

See below for more information about `config.rc` and `patch-types.txt`.

If you create a build-script for a common delta encoding tool that isn't
already supported, I'd gladly accept patches.

Configuration files
===================

There are a number of configuration files in the `config` directory.

config.rc
---------

This shell script should be sourced by every `patches/default.*.do` file, which
makes it a good place to set a custom `$PATH` to run delta encoding tools that
aren't installed system-wide.

patch-types.txt
---------------

This file lists file-extensions of patch types that should be included in the
final report. This extra level of customisation was added because not everybody
has every delta encoding tool installed, so it seemed good to let people easily
turn off tools that aren't available without having to delete the specific
build script.

The script `patches/patch-list.txt.do` combines the test-names from the tests
directory and the file extensions from this file to produce the master list of
patches that will be generated, which is filtered with `patch-blacklist.sed`.

patch-blacklist.sed
-------------------

This sed script is run over the master list of possible patches to produce the
list of patches that will actually be included in the final report. If you have
a test case that you know causes pathological behaviour in a particular tool,
you can use this script to delete that particular combination from the master
list. 

[1]: https://github.com/apenwarr/redo "The redo build system"
